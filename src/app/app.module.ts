import { Component, CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { ButtonComponent } from './button/button.component';
import { TitleComponent } from './title/title.component';

@NgModule({
  declarations: [
    ButtonComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
})
export class AppModule {
  constructor(private injector: Injector) {}

  injectCustomElement(name: string, component: any) {
    const el = createCustomElement(component, {
      injector: this.injector
    });

    customElements.define(name, el);
  }

  ngDoBootstrap() {
    this.injectCustomElement('ui-button', ButtonComponent);
    this.injectCustomElement('ui-title', TitleComponent);
  }
}
