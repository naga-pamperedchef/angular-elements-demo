import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {

  @Input() buttonLabel: string = '';

  @Output() tapEvent: EventEmitter<string> = new EventEmitter();

  constructor(private dataService: DataService) { }

  onClick() {
    this.tapEvent.emit('Button is tapped');
    this.dataService.updateTitleText('Some text is changed');
  }

}
