import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() {
    console.log('DataService is injected');

    this.titleText$.subscribe(() => {
      console.log('title text is changed')
    })

  }

  titleText$: BehaviorSubject<string> = new BehaviorSubject('');

  updateTitleText(newText: string) {
    this.titleText$.next(newText);
  }
}
